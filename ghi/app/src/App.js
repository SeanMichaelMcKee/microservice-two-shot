import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import { useEffect, useState } from 'react';

function App(props) {
  const [hats, setHats] = useState([])

  const getHats = async () => {
    const url = "http://localhost:8090/api/hats"
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      console.log(data)
      const hats = data
      // hats was not defined, so data.hats returned nothing
      setHats(hats)
    }
  }

  useEffect(() => {
    getHats();

  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatsList hats={hats} getHats={getHats} />} />
            <Route path="new" element={<HatsForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;


// const getHats = async () => {
//   const url = "http://localhost:8090/api/hats"
//   const response = await fetch(url);

//   if (response.ok) {
//     const data = await response.json();
//     const hats = data.hats
//     setHats(hats)
//   }
// }

// useEffect(() => {
//   getHats();

// }, [])


// return (
//   <BrowserRouter>
//     <Nav />
//     <div className="container">
//       <Routes>
//         <Route path="/" element={<MainPage />} />
//         <Route path="shoes" element={<HatsList hats={hats} />} />
//         <Route path="new" element={<HatForm />} />
//       </Routes>
//     </div>
//   </BrowserRouter>
// );
// }
