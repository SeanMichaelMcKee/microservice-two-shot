import React, {useEffect, useState} from 'react';
import { Link } from "react-router-dom"


async function deleteHat(hat) {
    const hatUrl = `http://localhost:8090/api/hats/${hat.id}/`
    const fetchConfig = {
        method: "delete",
        headers: {
          'Content-Type': 'application/json',
        },
      };
    await fetch(hatUrl, fetchConfig);
    
}

function HatsList({hats, getHats}) {
    if (hats === undefined) {
        return null;
    }
    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Location</th>
                <th>Fabric</th>
                <th>Style Name</th>
                <th>Color</th>0
            </tr>
            </thead>
            <tbody>
            {hats.map(hat => {
                console.log(hats);
                return (
                <tr key={hat.id}>
                    <td>{ hat.location }</td>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.style_name }</td>
                    <td>{ hat.color }</td>
                    <td>
                        <img
                            src={hat.picture}
                            alt=""
                            width="90"
                            height="90"
                            />
                    </td>
                    <td><button type="button" className="btn btn-danger" onClick={() => deleteHat(hat)}>Delete</button> </td>
                </tr>
                );
            })}

            </tbody>
        </table>
    );
}




export default HatsList;
