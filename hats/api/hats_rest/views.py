from django.shortcuts import render
from .models import LocationVO, Hat
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number", "import_href"]

    def check(self, o):
        return {"details": f"{o.closet_name} - section {o.section_number} - shelf {o.shelf_number}" }


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
    "id",
    "style",
    "fabric",
    "color",
    "picture_url",

       ]

    def check1(self, o):
        return {"location": o.location.import_href}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
    "style",
    "fabric",
    "color",
    "picture_url",
    "location",
    "id"
    ]

    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            hats,
            encoder=HatListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        print(content)

        try:
            location_href = content['location']
            location = LocationVO.objects.get(id=location_href)
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def show_hat(request, pk):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
